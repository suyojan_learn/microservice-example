package com.learn.microservice.orderservice.controller;


import com.learn.microservice.orderservice.entity.Order;
import com.learn.microservice.orderservice.model.MSMessage;
import com.learn.microservice.orderservice.model.OrderList;
import com.learn.microservice.orderservice.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api")
public class OrderController
{
    @Autowired
    private OrderRepository orderRepository;

    @PostMapping("/place-order")
    public ResponseEntity<MSMessage> placeOrder(@RequestBody Order order)
    {
        Order myorder = orderRepository.saveAndFlush(order);
        if (myorder.getOrderid() != 0)
            return ResponseEntity.ok(new MSMessage("Order placed successfully",1030,"Success"));
        else
            return ResponseEntity.ok(new MSMessage("Order Not Placed",2030,"Failed"));
    }

    @GetMapping("/find-order")
    public ResponseEntity<OrderList> fetchOrder(@RequestParam("userid")String userid)
    {
        List<Order> orders=orderRepository.findByUserid(userid);
        OrderList orderList = new OrderList();
        orderList.setOrderList(orders);
        return ResponseEntity.ok(orderList);
    }
}
