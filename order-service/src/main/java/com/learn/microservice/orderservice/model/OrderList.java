package com.learn.microservice.orderservice.model;

import com.learn.microservice.orderservice.entity.Order;

import java.util.List;

public class OrderList
{
    private List<Order> orderList;

    public List<Order> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<Order> orderList) {
        this.orderList = orderList;
    }
}
