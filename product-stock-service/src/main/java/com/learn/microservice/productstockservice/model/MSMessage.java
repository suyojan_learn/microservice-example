package com.learn.microservice.productstockservice.model;

public class MSMessage {
    private String message;
    private long errorCode;
    private String title;

    public MSMessage() {
    }

    public MSMessage(String message, long errorCode, String title) {
        this.message = message;
        this.errorCode = errorCode;
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(long errorCode) {
        this.errorCode = errorCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
