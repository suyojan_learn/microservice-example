package com.learn.microservice.productstockservice.repository;

import com.learn.microservice.productstockservice.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product,Long>
{
    Product findByProductCode(String code);
}
