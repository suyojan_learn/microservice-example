<h1>Description</h1>
This is spring boot based microservice example having four services running in parallel. 

The services are
<ul>
   <li>user-management : Provide login and registration service</li>
   <li>product-stock-service : Provide stock management service</li>
   <li>order-service : Provide order management service</li>
   <li>ui-server : Provide centra server to which the client will interact. It provide user interface to interact with the system.</li>
</ul>


All the services are communicating using <a href="https://github.com/Netflix/eureka">Eureka</a> based service discovery

discovery-server:- provide service registry and service discovery functionalities.


<h1>Steps to run the system</h1>

First create following databases:

1) usermanagement :- To manage user in the system
2) stock :- maintain the stock of products
3) order :- managing orders.


After the database configuration:


Start discover-server first so that discovery can be made possible
After that start other services in any order

To start any of the service, navigate to respective directory and use <b><i>mvn clean install</i></b>

<i>Note :- Maven should be installed on the system. Read <a href="https://maven.apache.org/">Maven</a> to know about maven</i>


After all service get started use 

http://localhost:8080 

to interact with the system

<h3>References</h3>
To read more about microservices and spring boot
<ol>
  <li><a href="https://microservices.io/">Microservices</a></li>
  <li><a href="https://github.com/Netflix/eureka">Eureka</a></li>
  <li><a href="https://spring.io/guides/gs/service-registration-and-discovery/">Service Registration and Discovery</a></li>
  <li><a href="https://spring.io/blog/2015/01/20/microservice-registration-and-discovery-with-spring-cloud-and-netflix-s-eureka">Microservice Registration and Discovery with Spring Cloud and Netflix's Eureka</a></li>
</ol>
